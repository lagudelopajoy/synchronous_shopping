<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Helper;

define('MAX_SHOPS', pow(10, 3));
define('MIN_SHOPS', 2);
define('MAX_PATHS', 2 * pow(10, 3));
define('MIN_PATHS', 1);
define('MAXTYFISH', 10);
define('MIMTYFISH', 1);
define('MAX_TIME',  pow(10, 4));


class PathController extends Controller
{
    protected $params;
    protected $typesOfFish         = [];
    protected $paths               = [];
    protected $mergeTypeOfFish     = [];
    protected $shoppingCenters     = [];
    protected $repeatedShopNames   = [];
    protected $nPaths              = 0;
    protected $numberPosibleRoutes = 0;

    public function index(Request $request){
        try {
            $this->params = $request->json()->all();

            if(is_null($this->params) || empty($this->params)) return response()->json(['error' => 'No data received']);

            if(!empty($this->validation())){
                return $this->validation();
            }

            $shortestPath = Helper::shortest_path($this->shoppingCenters, $this->numberPosibleRoutes);

            return response()->json([
                'shortest_path' => $shortestPath
            ]);
        }
        catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage
            ]);
        }
    }

    public function validation(){
        foreach($this->params as $line => $value){
            $aParams = explode(' ', $value);
            $line    = intval($line);

            if($line == 0){ #First line definitions
                if(count($aParams) !== 3){ return response()->json(['error' => 'Wrong number of parameters N, M, K']); }

                $N = intval($aParams[0]); #Number of shopping centers
                $M = intval($aParams[1]); #Number of roads
                $K = intval($aParams[2]); #Number of types of fish

                if($N > MAX_SHOPS || $N < MIN_SHOPS){
                    return response()->json(['error' => 'Wrong number of shopping centers, it must be between: ' . MIN_SHOPS . ' and ' . MAX_SHOPS]);
                }

                if($M > MAX_PATHS || $M < MIN_PATHS){
                    return response()->json(['error' => 'Wrong number of paths, it must be between: ' . MIN_PATHS . ' and ' . MAX_PATHS]);
                }

                if($K > MAXTYFISH || $K < MIMTYFISH){
                    return response()->json(['error' => 'Wrong number of paths, it must be between: ' . MIMTYFISH . ' and ' . MAXTYFISH]);
                }
            }

            if($line > 0 && $line <= $N){
                if(count($aParams) !== 2){ return response()->json(['error' => 'Wrong number of parameters ith']); }

                $this->typesOfFish[$line] = [
                    'number' => intval($aParams[0]), #Number of types of fish 
                    'type'   => intval($aParams[1]), #Type of fish 
                ];

                if($this->typesOfFish[$line]['number'] > $K || $this->typesOfFish[$line]['number'] < 0){
                    return response()->json(['error' => "Wrong number of types of fish, it must be between: 0 and $K"]);
                }

                if($this->typesOfFish[$line]['type'] > $K || $this->typesOfFish[$line]['type'] < 1){
                    return response()->json(['error' => "Wrong types of fish, it must be between: 1 and $K"]);
                }
                
                $this->mergeTypeOfFish[$this->typesOfFish[$line]['type']] = 1;
            }


            if($line > $N && $this->nPaths < $M){
                if(count($aParams) !== 3){ return response()->json(['error' => 'Wrong number of parameters jth']); }

                $this->paths[$this->nPaths] = [
                    'shopA' => intval($aParams[0]),
                    'shopB' => intval($aParams[1]),
                    'time'  => intval($aParams[2]),
                ];

                if(($this->paths[$this->nPaths]['shopA'] > $N || $this->paths[$this->nPaths]['shopA'] < 1) || ($this->paths[$this->nPaths]['shopB'] > $N || $this->paths[$this->nPaths]['shopB'] < 1)){
                    return response()->json(['error' => "Wrong number of shopping centers, it must be between: 1 and $N"]);
                }

                if($this->paths[$this->nPaths]['time'] > MAX_TIME || $this->paths[$this->nPaths]['time'] < 1){
                    return response()->json(['error' => "Wrong amount time for path, it must be between: 1 and " . MAX_TIME]);
                }

                if($this->paths[$this->nPaths]['shopA'] == $this->paths[$this->nPaths]['shopB']){
                    return response()->json(['error' => "Shopping centers must different"]);
                }

                $this->shoppingCenters[$this->nPaths] = new ShoppingCenter();
                $this->shoppingCenters[$this->nPaths]->address     = $this->paths[$this->nPaths]['shopA'];
                $this->shoppingCenters[$this->nPaths]->visited     = false;
                $this->shoppingCenters[$this->nPaths]->typeOfFish  = $this->typesOfFish[$this->nPaths + 1]['type'];
                $this->shoppingCenters[$this->nPaths]->timeInPath  = $this->paths[$this->nPaths]['time'];
                $this->shoppingCenters[$this->nPaths]->next        = $this->paths[$this->nPaths]['shopB'];

                if($this->paths[$this->nPaths]['shopA'] == 1){ $this->shoppingCenters[$this->nPaths]->head  = true; }
                if($this->paths[$this->nPaths]['shopB'] == $M){ $this->shoppingCenters[$this->nPaths]->tail = true; }

                if(!isset($this->repeatedShopNames[$this->paths[$this->nPaths]['shopA']])){
                    $this->repeatedShopNames[$this->paths[$this->nPaths]['shopA']] = 0;
                }

                $this->repeatedShopNames[$this->paths[$this->nPaths]['shopA']] += 1;
                if($this->repeatedShopNames[$this->paths[$this->nPaths]['shopA']] > 1 && $this->repeatedShopNames[$this->paths[$this->nPaths]['shopA']] > $this->numberPosibleRoutes){
                    $this->numberPosibleRoutes = $this->repeatedShopNames[$this->paths[$this->nPaths]['shopA']];
                }

                $this->nPaths += 1;

            }
        }

        if(count($this->mergeTypeOfFish) !== $K){
            return response()->json(['error' => "Wrong types of fish, there must be: $K types of fish and there are: " . count($this->mergeTypeOfFish)]);
        }
        #Fin de las validaciones
    }
}

#Debe existir por lo menos una ruta
#Debe haber un punyto de partida y final 
#Si existen dos rutas con el mismo Shpping center, aumenta el numero de rutas posibles
class ShoppingCenter{
    public $address;
    public $previous;
    public $next;
    public $visited;
    public $typeOfFish;
    public $timeInPath;
    public $head;
    public $tail;
}
    

