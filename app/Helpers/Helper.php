<?php 
    namespace App\Helpers;

    class Helper{
        public static $posibleRoutes  = [];
        public static $current        = 1;
        public static $lessTime       = 0;
        public static $route          = [];
        public static $shops;
        public static $posiblities;
        public static $shortPathJumps;
        public static $shortPath;

        public static function shortest_path($shops, $numberPosibleRoutes){
            self::$shops           = $shops;
            self::$posiblities     = $numberPosibleRoutes;
            self::$shortPathJumps = count(self::$shops);

            for($i = 0; $i < self::$posiblities; $i++){
                array_walk_recursive(self::$shops, 'Helper::find', $i);
                self::$current = 1;

                if(count(self::$posibleRoutes[$i]) < self::$shortPathJumps){
                    self::$shortPathJumps = count(self::$posibleRoutes[$i]);
                    self::$shortPath = $i;
                }
            }

            array_walk_recursive(self::$posibleRoutes[self::$shortPath], 'Helper::sum');

            return self::$lessTime;
        }

        public static function sum($jumps, $index){
            if($index == 'time'){
                self::$lessTime += $jumps;
            }
        }

        public static function find(&$shop, $index, $posibility){
            if($shop->address == self::$current && !$shop->visited){
                self::$posibleRoutes[$posibility][] = ['route' => $shop->address, 'time' => $shop->timeInPath];
                $shop->visited                      = true;
                self::$current                      = $shop->next; 

                if($shop->tail == true){
                    self::$posibleRoutes[$posibility][] = ['route' => $shop->address, 'time' => $shop->timeInPath];
                }
            }
        }
    }
