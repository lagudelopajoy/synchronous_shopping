# En este documento describo lo que realicé durante estos 7 días

## Capas de aplicación

Al ser esta una aplicación que sólo tiene una funcionalidad la cual es encontrar el menor tiempo que tomará a los gatos comprar todo el pescado
unicamente cuenta con una capa de negocio (lógica).

1. Esta compuesta por una clase prncipal (Controlador), el cual recibe las peticiones, se validan y se responde con valor resultante, o de lo contrario
responde con mensajes de error. 

El formato de la entrada es el siguiente:

```
$this->json = [
    '5 5 5',
    '1 1',
    '1 2',
    '1 3',
    '1 4',
    '1 5',
    '1 2 10',
    '1 3 10',
    '2 4 10',
    '3 5 10',
    '4 5 10',
];
```

Y el formato de la respuesta es: 

```
return response()->json([
    'shortest_path' => $shortestPath
]);
```

2. Una clase Helper que realiza las operaciones necesarias para encontrar el valor deseado y retornarlo al controlador.

## Pruebas

La prueba se puede ejecutar corriendo el siguiente comando:

```
$ vendor/bin/phpunit 
PHPUnit 5.7.21 by Sebastian Bergmann and contributors.

...                                                                 3 / 3 (100%)

Time: 61 ms, Memory: 10.00MB

OK (3 tests, 3 assertions)
```

Dicha prueba valida que la respuesta sea: 


```
[
    'shortest_path' => 30,
]
```
