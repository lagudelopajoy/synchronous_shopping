<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PathTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    protected $json;

    public function testExample()
    {
        $this->json = [
            '5 5 5',
            '1 1',
            '1 2',
            '1 3',
            '1 4',
            '1 5',
            '1 2 10',
            '1 3 10',
            '2 4 10',
            '3 5 10',
            '4 5 10',
        ];

        $this->json('POST', '/', $this->json)
            ->assertExactJson([
                'shortest_path' => 30,
            ]); 
    }
}
