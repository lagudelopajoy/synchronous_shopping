# Punto refactor

## Malas prácticas

1. En el código habían comentarios con código por dentro, estos se deben remover, de esta forma el código se ve más limpio.
2. Se debe reducir el uso de IFs anidados, ya que esto dificulta la lectura del código.
3. Evitar el llamado iterativo a funciones cuyo valor será usado varias veces, definir para esto una sola variable.
4. Evitar usar arreglos en línea dentro de una función con varios parámetros, esto dificulta la lectura del código.
5. Se debería incluir un comentario que describa la función.

## La organización de mi código

1. Se eliminaron los comentarios innecesarios.
2. Se eliminaron los IFs anidados.
3. Se define una variable $driver_id para ser utilizada varia veces.
4. Se define el arreglo $serviceId, para ser utilizado en la llamda a los métodos Push::ios, Push::android2.
5. Se agrega un comentario que describe la función
