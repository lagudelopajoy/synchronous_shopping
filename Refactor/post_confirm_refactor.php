<?php
    class SomeClass {

        /**
         * @desc this method allows sending push notification to users (Android and iPhone)
         * @author Luis Agudelo angelus628@gmail.com
         * @return json object
         */
        public function post_confirm(){
            $id       = Input::get('service_id');
            $servicio = Service::find($id);

            if(is_null($servicio)){
                return Response::json(array('error' => '3'));
            }

            if($servicio->status_id == '6'){
                return Response::json(array('error' => '2'));
            }

            if($servicio->driver_id !== NULL && $servicio->status_id !== '1'){
                return Response::json(array('error' => '1'));
            }

            if($servicio->user-uuid == ''){
                //No deberia ser un error diferente de cero?
                return Response::json(array('error' => '0'));
            }

            $driver_id = Input::get('driver_id');

            $servicio = Service::update($id, array(
                'driver_id' => $driver_id,
                'status_id' => 2
            ));

            Driver::update($driver_id, array(
                'available' => '0'
            ));

            $driverTmp = Driver::find($driver_id);
            Service::update($id, array(
                'card_id' => $driverTmp->card_id
            ));

            //Notificar usuario
            $pushMessage = 'Tu servicio ha sido confirmado!';
            $push        = Push::make();
            $serviceId   = ['serviceId' => $servicio->id];

            if($servicio->user->type == '1'){ //iPhone
                $result = $push->ios($servicio->user->uuid, $pushMessage, 1, 'honk.wav', 'Open', $serviceId);
            } 
            else { //Android
                $result = $push->android2($servicio->user->uuid, $pushMessage, 1, 'default', 'Open', $serviceId);
            }

            return Response::json(array('error' => '0'));
        }
    }
    
